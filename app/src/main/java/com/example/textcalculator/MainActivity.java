package com.example.textcalculator;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void click(View view) {
        try {
            startActivity(
                    new Intent(this, Result.class)
                            .putExtra("a",
                                    Integer.parseInt(
                                            String.valueOf(
                                                    ((EditText) findViewById(R.id.a))
                                                            .getText()
                                            )
                                    )
                            )
                            .putExtra("b",
                                    Integer.parseInt(
                                            String.valueOf(
                                                    ((EditText) findViewById(R.id.b))
                                                            .getText()
                                            )
                                    )
                            )
            );
            ((TextView) findViewById(R.id.error)).setError("",null);
        } catch (NumberFormatException e) {
            ((TextView) findViewById(R.id.error)).setError("Ошибка");
        }
    }
}